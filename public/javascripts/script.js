$(document).ready(function () {
    $('.addRow').on('click', function () {
        $('tbody').append('<tr><td>'+$('#text').val()+'</td><td>' + moment(Date.now()).format('MMMM Do YYYY, h:mm:ss')+'</td></tr>');
        $('#text').val('');
    });

});
